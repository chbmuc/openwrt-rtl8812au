#!/bin/bash
VERS=$1
TARGET=$2
ARCH=$3
CROSS="gcc-7.3.0_musl.Linux-x86_64"

usage() {
	echo "usage: buidl.sh <version> <target> <architecture>"
	exit 1
}

if [ $# != 3 ] ; then
	usage
fi

if [ "$VERS" == "snapshot" ] ; then
	URL="https://downloads.openwrt.org/snapshots/targets/${TARGET}/${ARCH}/openwrt-sdk-${TARGET}-${ARCH}_${CROSS}.tar.xz"
else
	URL="https://downloads.openwrt.org/releases/${VERS}/targets/${TARGET}/${ARCH}/openwrt-sdk-${VERS}-${TARGET}-${ARCH}_${CROSS}.tar.xz"
fi

# Fetch OpenWRT-SDK
curl "$URL" | tar -Jxv --strip 1

# Apply backport patch
cat include_net_cfg80211.patch | ( cd build_dir/target-*/linux-*/linux-*/ ; patch -p1 )

# Build package
make defconfig
make package/kernel/rtl8812au/compile
